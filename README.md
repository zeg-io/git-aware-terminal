#Git Aware Terminal
This is the first node version of the original project we published as a shell script.  The idea was to make it more accessible and easy to install for folks

The link to the original shell script version is at the bottom of this readme.
##About

This adds pretty flags to your terminal _only_ for directories which are git enabled.

It displays all manner of information about the git status so you don't have to constantly type `git status` as you are working in the terminal.  We feel it's so great, we really just can't go back to working without it!

We just cannot go back to life without it, and we think that after you try it, you won't want to either.

Enjoy!

##Demo
![Demo](https://bytebucket.org/zeg-io/git-aware-terminal/raw/5d24026faab5b914022b252b17d0f1097a3d93d8/img/demo.gif)
##Supported OS's
Currently we have built in support for **macOS and Linux**. Getting the rest to work is mostly a matter of validating what is used for .bash_profile and how to execute on prompt. Drop us a note and let us know what OS you want worked on next

If you're itching to have Windows working, we'd be glad to include your contributions.

##Installation
Installation is simple.  Run the commands below, that should be it!
```shell
npm install -g git-aware-terminal
gat install
```

You may need to reload your bash profile or just restart your terminal.

**IMPORTANT NOTE: Unless you have navigated to a directory that is git enabled you will see no difference in your terminal.**

###Original shell-script version
We published our initial work at https://github.com/zeg-io/git-aware-terminal.