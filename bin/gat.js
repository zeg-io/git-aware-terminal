#! /usr/bin/env node

let config = require('../config'),
    path = require('path'),
    spawn = require('child_process').spawn,
    install = require('./install'),
    Buffer = require('buffer').Buffer

let baseDir = path.resolve('.'),
    fg = config.colors,
    bg = fg.bg

// Useful variables for building things with
let branchName = '',
    branchSymbol = '',
    branchErrMessage = '',
    branchRelativePosition = '', // ahead/behind/unpushed
    branchDistanceFromMaster = 0,
    branchDiverged = false,
    stagedFileCount = 0,
    unstagedFileCount = 0,
    untrackedFileCount = 0,
    thisThis = this

let git = function (command, then) {
  let stdOut = [],
      stdErr = [],
      gitOptions = command.split(' ')

  let gitCmd = spawn('git', gitOptions, { cwd: baseDir })

  gitCmd.stdout.on('data', buffer => stdOut.push(buffer))
  gitCmd.stderr.on('data', buffer => stdErr.push(buffer))
  gitCmd.on('error', err => stdErr.push(new Buffer(err.stack, 'ascii')))
  gitCmd.on('close', (exitCode, exitSignal) => {
    delete this.childProcess

    if (exitCode && stdErr.length) {
      stdErr = Buffer.concat(stdErr).toString('utf-8')
      then.call(this, stdErr, null)
    } else {
      then.call(this, null, Buffer.concat(stdOut).toString('utf-8'))
    }
  })

  thisThis.childProcess = gitCmd

  if (thisThis.outputHandler) {
    thisThis.outputHandler(command,
        thisThis.childProcess.stdout,
        thisThis.childProcess.stderr)
  }
}

let buildOutput = () => {
  let sugar = bg.git_prompt + fg.git_prompt + ' git '

  if (untrackedFileCount > 0) {
    sugar += bg.untracked + fg.untracked + ' ' + untrackedFileCount + config.symbols.untracked
  }
  if (unstagedFileCount > 0) {
    sugar += bg.unstaged + fg.unstaged + ' ' + unstagedFileCount + config.symbols.unstaged
  }
  if (stagedFileCount > 0) {
    sugar += bg.staged + fg.staged + ' ' + stagedFileCount + config.symbols.staged
  }

  sugar += bg.commits + fg.commits

  if (branchDiverged) {
    sugar += bg.symbols.branch_diverged
  }
  if (branchRelativePosition) {
    if (branchRelativePosition === 'ahead') {
      sugar += ' ' + config.symbols.commit_ahead + branchDistanceFromMaster + config.symbols.branch_ahead + ' '
    } else if (branchRelativePosition === 'behind') {
      sugar += ' ' + config.symbols.commit_behind + branchDistanceFromMaster + config.symbols.branch_behind + ' '
    } else if (branchRelativePosition === 'unpushed') {
      sugar += ' ' + config.symbols.branch_ahead + ' '
    }
  }
  if (branchErrMessage !== '') {
    branchSymbol = fg.detatched + fg.bold + config.symbols.branch_detached
    branchName = branchErrMessage + config.symbols.branch_detached
  }
  sugar += bg.branch + fg.branch + branchSymbol + branchName + ' ' + fg.reset + '\n'

  process.stdout.write(sugar)
  process.exit()

}

let getFileStatusCounts = (fileStatus) => {
  let reStaged = /^[A-Z]/gm,
      reUnstaged = /^.[A-Z]/gm,
      reUntracked = /^\?\?/gm

  if (fileStatus) {
    let tmpMatch = fileStatus.match(reStaged)

    if (tmpMatch) {
      stagedFileCount = tmpMatch.length
    }

    tmpMatch = fileStatus.match(reUnstaged)
    if (tmpMatch) {
      unstagedFileCount = tmpMatch.length
    }

    tmpMatch = fileStatus.match(reUntracked)
    if (tmpMatch) {
      untrackedFileCount = tmpMatch.length
    }
  }
}

let getStatus = () => {
  let fileStatus = '',
      fullStatus = ''

  git('status --porcelain', (err, out) => {
    fileStatus = out

    git('status', (err, out) => {
      fullStatus = out

      let re = /.*branch.*(ahead|behind).*(\d).*commit/g,
          match = re.exec(fullStatus),
          reDiverged = /.*branch.*diverged/g

      if (match) {
        branchRelativePosition = match[1]
        branchDistanceFromMaster = match[2]
        branchDiverged = fullStatus.match(reDiverged)

        // now the file status
        getFileStatusCounts(fileStatus)
        buildOutput()
      } else {
        if (fullStatus.indexOf('up-to-date') < 0) {
          git('log', (err, out) => {
            if (out) {
              branchDistanceFromMaster = 0
              branchRelativePosition = 'unpushed'
            }

            // now the file status
            getFileStatusCounts(fileStatus)
            buildOutput()
          })
        } else {
          getFileStatusCounts(fileStatus)
          buildOutput()
        }
      }
    })
  })
}

let gatError = (message, exit) => {
  let sugar = bg.git_prompt + fg.git_prompt + ' git ' + bg.branch + fg.detatched + fg.bold + config.symbols.branch_detached

  sugar += message
  sugar += config.symbols.branch_detached + ' ' + fg.reset + '\n'
  process.stdout.write(sugar)

  if (exit) {
    process.exit()
  }
}

let getBranchName = () => {
  git('rev-parse --abbrev-ref HEAD', (err, out) => {
    if (err) {
      if (err.indexOf("ambiguous argument \'HEAD\'") > 0) {
        git('remote -v', (err, out) => {
          if (out) {
            git('status', (err, out) => {
              let reBranch = /^On branch (\w+)/g,
                  match = reBranch.exec(out)

              if (match) {
                branchSymbol = fg.bold + fg.detatched + config.symbols.branch_detached
                branchName = match[1]
              } else {
                branchErrMessage = 'no branch created as there are no commits'
              }
            })
          } else {
            branchErrMessage = 'no origin set yet'
          }
        })

        getStatus()
      }
    }
    if (out) {
      branchName = out.replace('\n', '')

      if (branchName === 'HEAD') {
        branchName = `${config.colors.bold}detached${config.symbols.branch_detached}`
      } else {
        git(`branch -r --list origin/${branchName}`, (err, out) => {
          out = out.replace('\n', '').trim()
          if (out === null || out === '') {
            branchSymbol = fg.detatched + fg.bold + config.symbols.branch
          } else {
            branchSymbol = config.symbols.branch
          }
          getStatus()
        })
      }
    }
  })
}

// BEGIN ///////
if (process.argv.length > 2) {
  switch (process.argv[2]) {
    case 'i':
    case 'install':
    case '-i':
    case '--install':
      install()
      break
    default:
      console.log('Unknown argument. Expects either no argument or "install"')
      break
  }
} else {
  getBranchName()
}
