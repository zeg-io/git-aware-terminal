/**
 * Created by tonyarcher on 10/11/16.
 */

let config = require('../config'),
    fs = require('fs'),
    path = require('path'),
    readline = require('readline')

let os = process.platform,
    homeDir = process.env.HOME || process.env.USERPROFILE,
    bashConfig = {
      darwin: homeDir + '/.bash_profile',
      freebsd: '',
      linux: homeDir + '/.bash_profile',
      sunos: '',
      win32: ''
    }

let log = (message) => process.stdout.write(message)

let spliceSlice = (str, index, count, add) => {
  // We cannot pass negative indexes directly to the 2nd slicing operation.
  if (index < 0) {
    index = str.length + index;
    if (index < 0) {
      index = 0;
    }
  }
  return str.slice(0, index) + (add || "") + str.slice(index + count);
}

let reloadBash = () => {
  // now refresh the bash profile
  let exec = require('child_process').exec;

  exec(`source ${bashConfig[os]}`, function(error, stdout, stderr) {
    // command output is in stdout
    console.log(`executed: source ${bashConfig[os]}`)
    console.log(stdout)
  });
}

let installer = () => {
  if (os !== 'darwin' && os !== 'linux') {
    log(config.colors.red + `Your operating system [${os}] is not currently supported by Git Aware Terminal.`)
    log(config.colors.red + 'Currently we only support macOS. Help contribute to the project and add support for other operating systems!')
    log(config.colors.reset + '\n')
  } else {
    bashProfile = fs.readFileSync(bashConfig.darwin).toString().trim()

    let re = /^PROMPT_COMMAND.*/gm,
        match = re.exec(bashProfile),
        newPromptCommand = `\nPROMPT_COMMAND="node ${path.join(__dirname, '/gat.js')}"`

    if (match) {
      log(config.colors.detatched + `We noted you already have a PROMPT_COMMAND in your ${bashConfig[os]} file.\n`)
      var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      });
      log(config.colors.reset)
      rl.question(`Would you like to comment out [${config.colors.git_prompt}${match[0]}${config.colors.reset}] and enable Git Aware Terminal? (y/N) `, (response) => {
        response = response.toLowerCase()
        if (response === 'y' || response === 'yes') {
          let idx = match.index
          let newBashProfile = spliceSlice(bashProfile, idx, 0, '#')
          newBashProfile = spliceSlice(newBashProfile, idx + 1 + match[0].length, 0, newPromptCommand)

          fs.writeFileSync(bashConfig[os], newBashProfile)

          process.exit()
        } else {
          process.exit()
        }
      })

    } else {
      // no existing prompt command, add it to the end
      newBashProfile = bashProfile + newPromptCommand
      fs.writeFileSync(bashConfig[os], newBashProfile)
    }
  }
}

module.exports = installer
