/**
 * Created by tonyarcher on 10/11/16.
 */

let tcolor = {
  white: '\033[38;5;255m',
  off_white: '\033[38;5;15m',
  black: '\033[38;5;232m',
  red: '\033[38;5;160m',
  bg: {
    off_white: '\033[48;5;7m',
    blue: '\033[48;5;32m',
    red: '\033[48;5;160m',
    green: '\033[48;5;22m',
    orange: '\033[48;5;166m',
    purple: '\033[48;5;53m'
  },
  bold: '\033[1m',
  reset: '\033[0m'
}

let config = {
  colors: {
    git_prompt: tcolor.off_white,
    detatched: tcolor.red,
    untracked: tcolor.white,
    staged: tcolor.white,
    unstaged: tcolor.white,
    branch: tcolor.black,
    commits: tcolor.white,
    bg: {
      git_prompt: tcolor.bg.blue,
      untracked: tcolor.bg.red,
      staged: tcolor.bg.green,
      unstaged: tcolor.bg.orange,
      branch: tcolor.bg.off_white,
      commits: tcolor.bg.purple
    },
    bold: tcolor.bold,
    reset: tcolor.reset
  },
  symbols: {
    untracked: '። ',
    staged: '⚑ ',
    unstaged: 'Δ ',
    commit_behind: '-',
    commit_ahead: '+',
    branch: ' ᚶ ',
    branch_detached: ' ✇ ',
    branch_diverged: '⇡⇣ ',
    branch_ahead: '⇡',
    branch_behind: '⇣'
  }
}

module.exports = config
